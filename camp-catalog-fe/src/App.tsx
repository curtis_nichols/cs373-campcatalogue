import React from "react";
import "./App.css";
import { Navbar, Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route} from "react-router-dom";
import {HashRouter} from "react-router-dom";
import Parks from "./modelPages/Parks";
import Wildlife from "./modelPages/Wildlife";
import History from "./modelPages/History";
import About from "./About";
import ParkPage from "./instancePages/ParkPage";
import WildlifePage from "./instancePages/WildlifePage";
import HistoryPage from "./instancePages/HistoryPage";
import Splash from "./Splash";
import Search from "./Search";
import {LinkContainer} from "react-router-bootstrap";
import * as serviceWorker from "./serviceWorker";
import Visualizations from "./Visualizations";
import ProviderVisualizations from "./ProviderVisualizations";
import { useQuery, QueryCache, ReactQueryCacheProvider } from 'react-query';
import Favicon from 'react-favicon';
import logo from './img/cc-logo.jpg';


const queryCache = new QueryCache()


function App() {
  
serviceWorker.unregister();
  
  return (
    <ReactQueryCacheProvider queryCache={queryCache}>
      <HashRouter>
        {/* Navbar with the splash page, about page, search page, and model pages */}
        <Navbar style={{backgroundColor: '#1B4628', color: "#F0F0A1"}}>
          <Navbar.Brand style={{color: "#F0F0A1"}} href="/">
            Camp Catalog
          </Navbar.Brand>
          <Nav className="mr-auto">
            <LinkContainer style={{color: "#F0F0A1"}} to="/about">
              <Nav.Link>About</Nav.Link>
            </LinkContainer>
            <LinkContainer style={{color: "#F0F0A1"}} to="/parks">
              <Nav.Link>Parks</Nav.Link>
            </LinkContainer>
            <LinkContainer style={{color: "#F0F0A1"}} to="/wildlife">
              <Nav.Link>Wildlife</Nav.Link>
            </LinkContainer>
            <LinkContainer style={{color: "#F0F0A1"}} to="/history">
              <Nav.Link>History</Nav.Link>
            </LinkContainer>
            <LinkContainer style={{color: "#F0F0A1"}} to="/visualizations">
              <Nav.Link>Visualizations</Nav.Link>
            </LinkContainer >
            <LinkContainer style={{color: "#F0F0A1"}} 
            to="/provider-visualizations">
              <Nav.Link>Provider Visualizations</Nav.Link>
            </LinkContainer>
            <LinkContainer style={{color: "#F0F0A1"}} to="/search">
              <Nav.Link>Search</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar>
        <div className="App">

          <Favicon url= {logo} />

          {/* Switch statement that routes the various components */}
          <Switch>
            <Route path="/" exact component={Splash} />
            <Route path="/about" component={About} />
            <Route path="/parks" exact component={Parks} />
            <Route path="/wildlife" exact component={Wildlife} />
            <Route path="/history" exact component={History} />
            <Route path="/splash" component={Splash} />
            <Route path="/search" component={Search} />
            <Route path="/parks/:parkCode" component={ParkPage} />
            <Route path="/wildlife/:scientific_name" component={WildlifePage}/>
            <Route path="/history/:id" component={HistoryPage} />
            <Route path="/visualizations" component={Visualizations} />
            <Route path="/provider-visualizations" 
            component={ProviderVisualizations} />
          </Switch>
        </div>
      </HashRouter>
    </ReactQueryCacheProvider>
  );
}

export default App;
