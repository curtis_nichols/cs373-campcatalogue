import React, { Component } from "react";
import { Container, Row, Col, Image, Table } from "react-bootstrap";
import {Link} from "react-router-dom";
import { LinearProgress } from "@material-ui/core";
import Collapsible from "react-collapsible";
import { useQuery} from 'react-query'


/**
 * The ParkPage component is displayed
 * for each instance of a National Park
 */

function ParkPage (props: any)  {
  
  
  const code = props.match.params.parkCode;
  const { isLoading, error, data } = useQuery(`${code}`, () =>
    fetch(`/api/parks/${code}` ).then(res =>
      res.json()
    )
  )
    /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }


  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }
  else{

  var park = data;

  // Create lists of related instances from the other models,
  // based on what is in the data
  var historyList = park.related_historical_figures.map((person: any) =>
    <Link to={"/history/" + (person.id).toString()}>
      <Image fluid src={person.image} rounded/>
      <li>{person.full_name}</li>
    </Link>
  );
  var wildlifeList = park.related_wildlife.map((life: any) =>
    <Link to={"/wildlife/" + (life.scientific_name).replace("_", " ")}>
      <Image fluid src={life.image} rounded/>
      <li>{life.scientific_name}</li>
    </Link>
  );

  // Create HTML elements for the boolean values in the data
  var foodOffered = park.food ? <td>Yes</td> : <td>No</td>;
  var campingOffered = park.camping ? <td>Yes</td> : <td>No</td>;
  var hikingOffered = park.hiking ? <td>Yes</td> : <td>No</td>;

  return (
    <div className="App">
      <Container>
        <Row>
          <Col>
            <h1 className="text-justify">{park.name}</h1>
          </Col>
          <Col></Col>
        </Row>
        <Row>
          <Col>
            <p className="text-justify">
              {park.description}
            </p>
            <Image fluid 
              src={
              `https://api.mapbox.com/styles/v1/mapbox/light-v10/static/pin-s+FF0000(${park.longitude},${park.latitude})/${park.longitude},${park.latitude},5/500x300?access_token=pk.eyJ1IjoibmlraGlsYXRleGFzIiwiYSI6ImNrZ2ZxbDl6bTBxM3MyeHJzbXdqMms1cnYifQ.Npbk7aaH2HyWxaAOLkGyvQ`
              } />
          </Col>
          <Col>
            <Container>
              <Row>
                <Col>
                  <Image fluid src={park.image} />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Table className="table-dark" striped bordered hover>
                    <thead>
                      <tr>
                        <th colSpan={2}>{park.name}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Hours</td>
                        <td>{park.saturday_hours}</td>
                      </tr>
                      <tr>
                        <td>City</td>
                        <td>{park.city}</td>
                      </tr>
                      <tr>
                        <td>State</td>
                        <td>{park.state}</td>
                      </tr>
                      <tr>
                        <td>Zip Code</td>
                        <td>{park.zip}</td>
                      </tr>
                      <tr>
                        <td>Entry Fee</td>
                        <td>{park.fee}</td>
                      </tr>

                      <tr>
                        <td>Camping Offered</td>
                        <td>{campingOffered}</td>
                      </tr>
                      <tr>
                        <td>Hiking Offered</td>
                        <td>{hikingOffered}</td>
                      </tr>
                      <tr>
                        <td>Food Available</td>
                        <td>{foodOffered}</td>
                      </tr>
                      <tr>
                        <td>Related Historical Figures</td>
                        <Collapsible trigger="Click to Expand">
                          <td><ul>{historyList}</ul></td>
                        </Collapsible>
                      </tr>
                      <tr>
                        <td>Related Wildlife</td>
                        <Collapsible trigger="Click to Expand">
                          <td><ul>{wildlifeList}</ul></td>
                        </Collapsible>
                      </tr>
                    </tbody>
                  </Table>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    </div>
  );}
};

export default ParkPage;
