import { LinearProgress } from "@material-ui/core";
import React, { Component } from "react";
import { Container, Row, Col, Image, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Collapsible from "react-collapsible";
import { useQuery} from 'react-query'

/**
 * The WildlifePage component is displayed
 * for each instance of a wildlife datum.
 */


function WildlifePage (props: any)  {
  
  const name = props.match.params.scientific_name.replace("_", " ")
  const { isLoading, error, data } = useQuery(`${name}`, () =>
    fetch(`/api/wildlife/${name}` ).then(res =>
      res.json()
    )
  )
    /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }


  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }


   else{
    var life = data;

    // Some APIs require linking to their website
    // as a condition of using their images.
    // This section constructs HTML elements based
    // on the source of the image.
    var imageSrc = <></>;
    var photoCredit = <></>;
    if (life.photographer !== "")
      photoCredit = <tr>
        <td>Image Source</td>
        <td>{life.photographer}</td>
      </tr>;

    if (life.image_source == "eol.org")
      imageSrc = 
        <p>Image from <a href="https://eol.org/">Encyclopedia of Life</a></p>
    else if (life.image_source == "pexels.com")
      imageSrc = 
        <p>Image from <a href="https://www.pexels.com/">Pexels</a></p>
    else if (life.image_source == "commons.wikimedia.org")
      imageSrc = 
        <p>Image from <a href="https://commons.wikimedia.org/">
          Wikimedia Commons</a>
        </p>

    // Create an HTML list of parks related to this wildlife.
    var parkList = life.related_parks.map((park: any) =>
      <Link to={"/parks/" + (park.code).toString()}>
        <Image fluid src={park.image} rounded/>
        <li>{park.name}</li>
      </Link>
    );
  
    return (

      <div className="App">
      <Container>
        <Row>
          <Col>
          <h1 className="text-justify">{life.scientific_name}</h1>
          </Col>
          <Col></Col>
        </Row>
        <Row>
          <Col>
            <p className="text-justify">
              This animal is also know as: {life.common_names}.
            </p>
            <p>
              {imageSrc}
            </p>

          </Col>
          <Col>
            <Container>
              <Row>
                <Col>
                  {<Image fluid src={life.image} />}
                </Col>
              </Row>
              <Row>
                <Col>
                <Table className="table-dark" striped bordered hover>
                <tbody>
                  <tr>
                    <td>Category</td>
                    <td>{life.category}</td>
                  </tr>
                  <tr>
                    <td>Order</td>
                    <td>{life.order}</td>
                  </tr>
                  <tr>
                    <td>Family</td>
                    <td>{life.family}</td>
                  </tr>
                  <tr>
                    <td>Scientific Name</td>
                    <td>{life.scientific_name}</td>
                  </tr>
                  <tr>
                    <td>Nativeness</td>
                    <td>{life.nativeness}</td>
                  </tr>
                  {photoCredit}
                  <tr>
                    <td>Related Parks</td>
                    <Collapsible trigger="Click to Expand">
                      <td><ul>{parkList}</ul></td>
                    </Collapsible>
                  </tr>
                </tbody>
              </Table>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    </div>
    );
  }
}


export default WildlifePage;
