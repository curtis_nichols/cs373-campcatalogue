import { LinearProgress } from "@material-ui/core";
import React, { Component} from "react";
import { Container, Row, Col, Image, Table } from "react-bootstrap";
import {Link} from "react-router-dom";
import Collapsible from "react-collapsible";
import { useQuery } from 'react-query'

/**
 * The HistoryPage component is displayed
 * for each instance of a historical figure.
 *
*/

function HistoryPage(props: any) {

  const id = props.match.params.id;
  const { isLoading, error, data } = useQuery(`${id}`, () =>
    fetch(`/api/historical_people/${id}` ).then(res =>
      res.json()
    )
  )
    /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }


  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }



  else{
    var person = data;
    // Create a list of parks related to this historical figure,
    // based on what is in the data
    var historyList = person.related_parks.map((park: any) =>
      <Link to={"/parks/" + (park.code).toString()}>
        <Image fluid src={park.image} rounded/>
        <li>{park.name}</li>
      </Link>
    );
  return (
    <div className="App">
     <Container>
        <Row>
            <Col>
                <h1 className="text-justify">{person.full_name}</h1>
            </Col>
            <Col></Col>
        </Row>
        <Row>
            
            <Col>
                <p className="text-justify">
                {person.description}
                </p>
            </Col>
            <Col>
                <Container>
                  <Row>
                       <Col>
                          <Image fluid src={person.image} /> 
                      </Col>
                   </Row>
                   <Row>
            <Col>
                <Table className="table-dark" striped bordered hover>
                    <thead>
                        <tr>
                            <th colSpan={2}>
                              {person.first_name} {person.last_name}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Full Name</td>
                            <td>{person.full_name}</td>
                        </tr>
                        <tr>
                            <td>Born</td>
                            <td>{person.date_of_birth}</td>
                        </tr>
                        <tr>
                            <td>Place of Birth</td>
                            <td>{person.place_of_birth}</td>
                        </tr>
                        <tr>
                            <td>Died</td>
                            <td>{person.date_of_death}</td>
                        </tr>
                        <tr>
                            <td>Significance</td>
                            <td>{person.significance}</td>
                        </tr>
                        <tr>
                          <td>Related Parks</td>
                          <Collapsible trigger="Click to Expand">
                            <td><ul>{historyList}</ul></td>
                          </Collapsible>
                        </tr>
                    </tbody>
                </Table>
            </Col>
            </Row>
            </Container>
            </Col>
        </Row>
     </Container>
    </div>
  );
  }
}


export default HistoryPage;
