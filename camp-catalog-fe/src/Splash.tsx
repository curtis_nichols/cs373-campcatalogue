import React, { Component } from "react";
import landing_img from "./img/CC-landing.jpg";
import { Link } from "react-router-dom";
import { useSpring, animated} from "react-spring";

/**
 * The Splash component is displayed
 * in the home page, and can be accessed
 * again by clicking on the title of the
 * webpage in the navbar. It displays the 
 * landing logo and a button that allows
 * users to see the parks.
 */

  function Splash() {
    const fadeIn = useSpring({
      from: {
        opacity: 0
      },
      to: {
        opacity: 1
      },
      config: { 
        duration: 2000 
      }
    });

    return (
      <div
        className="App"
        style={{ height: '100vh', position: 'relative', 
        backgroundSize: 'cover', backgroundImage: "url(" + landing_img + ")" }}
      >
        <title>Splash</title>
        <h1
          style={{
            textAlign: "right",
            right: "10%",
            top: "25%",
            position: "relative",
            color: "white",
          }}
        >
          <animated.div style={fadeIn}>Welcome to Camp Catalog!</animated.div>
        </h1>
        <Link
          to="/parks"
          style={{
            textAlign: "right",
            left: "20%",
            top: "26%",
            position: "relative",
          }}
        >
          <button type="button" className="btn btn-outline-light">
            <animated.div style={fadeIn}>See the Parks</animated.div>
          </button>
        </Link>
      </div>
    );
  }


export default Splash;
