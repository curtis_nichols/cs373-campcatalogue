import React from "react";
import ReactDOM from "react-dom";
import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import FusionMaps from "fusioncharts/maps/fusioncharts.usa";
import World from "fusioncharts/fusioncharts.maps";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import ReactFusioncharts from "react-fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import { useSpring, animated } from "react-spring";

ReactFC.fcRoot(FusionCharts, FusionMaps, World, FusionTheme);
charts(FusionCharts);

const dataset = [{
    "id": "AL",
    "value": "9",
    "showLabel": "1"
}, {
    "id": "AK",
    "value": "19",
    "showLabel": "1"
}, {
    "id": "AZ",
    "value": "20",
    "showLabel": "1"
}, {
    "id": "AR",
    "value": "7",
    "showLabel": "1"
}, {
    "id": "CA",
    "value": "30",
    "showLabel": "1"
}, {
    "id": "CO",
    "value": "12",
    "showLabel": "1"
}, {
    "id": "CT",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "DE",
    "value": "1",
    "showLabel": "1"
}, {
    "id": "DC",
    "value": "28",
    "showLabel": "1"
}, {
    "id": "FL",
    "value": "10",
    "showLabel": "1"
}, {
    "id": "GA",
    "value": "12",
    "showLabel": "1"
}, {
    "id": "HI",
    "value": "9",
    "showLabel": "1"
}, {
    "id": "ID",
    "value": "5",
    "showLabel": "1"
}, {
    "id": "IL",
    "value": "2",
    "showLabel": "1"
}, {
    "id": "IN",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "IA",
    "value": "2",
    "showLabel": "1"
}, {
    "id": "KS",
    "value": "5",
    "showLabel": "1"
}, {
    "id": "KY",
    "value": "5",
    "showLabel": "1"
}, {
    "id": "LA",
    "value": "6",
    "showLabel": "1"
}, {
    "id": "ME",
    "value": "5",
    "showLabel": "1"
}, {
    "id": "MD",
    "value": "21",
    "showLabel": "1"
}, {
    "id": "MA",
    "value": "17",
    "showLabel": "1"
}, {
    "id": "MI",
    "value": "7",
    "showLabel": "1"
}, {
    "id": "MN",
    "value": "4",
    "showLabel": "1"
}, {
    "id": "MS",
    "value": "9",
    "showLabel": "1"
}, {
    "id": "MO",
    "value": "6",
    "showLabel": "1"
}, {
    "id": "MT",
    "value": "4",
    "showLabel": "1"
}, {
    "id": "NE",
    "value": "5",
    "showLabel": "1"
}, {
    "id": "NV",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "NH",
    "value": "1",
    "showLabel": "1"
}, {
    "id": "NJ",
    "value": "6",
    "showLabel": "1"
}, {
    "id": "NM",
    "value": "23",
    "showLabel": "1"
}, {
    "id": "NY",
    "value": "29",
    "showLabel": "1"
}, {
    "id": "NC",
    "value": "9",
    "showLabel": "1"
}, {
    "id": "ND",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "OH",
    "value": "11",
    "showLabel": "1"
}, {
    "id": "OK",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "OR",
    "value": "4",
    "showLabel": "1"
}, {
    "id": "PA",
    "value": "22",
    "showLabel": "1"
}, {
    "id": "RI",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "SC",
    "value": "10",
    "showLabel": "1"
}, {
    "id": "SD",
    "value": "6",
    "showLabel": "1"
}, {
    "id": "TN",
    "value": "8",
    "showLabel": "1"
}, {
    "id": "TX",
    "value": "14",
    "showLabel": "1"
}, {
    "id": "UT",
    "value": "12",
    "showLabel": "1"
}, {
    "id": "VT",
    "value": "2",
    "showLabel": "1"
}, {
    "id": "VA",
    "value": "25",
    "showLabel": "1"
}, {
    "id": "WA",
    "value": "11",
    "showLabel": "1"
}, {
    "id": "WV",
    "value": "7",
    "showLabel": "1"
}, {
    "id": "WI",
    "value": "3",
    "showLabel": "1"
}, {
    "id": "WY",
    "value": "6",
    "showLabel": "1"
}];

const colorrange = {
    "minvalue": "0",
    "code": "#FFE0B2",
    "gradient": "1",
    "color": [{
        "minvalue": "1",
        "maxvalue": "10",
        "color": "#FFD74D"
    }, {
        "minvalue": "10",
        "maxvalue": "20",
        "color": "#FB8C00"
    }, {
        "minvalue": "20",
        "maxvalue": "30",
        "color": "#E65100"
    }]
};

const chartConfigs = {
    type: 'usa', // The chart type
    width: '700', // Width of the chart
    height: '400', // Height of the chart
    dataFormat: 'json', // Data type
    dataSource: {
        // Map Configuration
        "chart": {
            "caption": "National Parks per State",
            "includevalueinlabels": "1",
            "labelsepchar": ": ",
            "entityFillHoverColor": "#FFF9C4",
            "theme": "fusion"
        },
        // Aesthetics; ranges synced with the slider
        "colorrange": colorrange,
        // Source data as JSON --> id represents state.
        "data": dataset
    }
}

const dataSource = {
    chart: {
      caption: "Abundance of Wildlife",
      plottooltext: 
      "<b>$percentValue</b> of animals in US national parks are $label",
      showlegend: "1",
      showpercentvalues: "1",
      legendposition: "bottom",
      usedataplotcolorforlabels: "1",
      theme: "fusion"
    },
    data: [
      {
        label: "Common",
        value: "33"
      },
      {
        label: "Uncommon",
        value: "41"
      },
      {
        label: "Rare",
        value: "13"
      },
      {
        label: "Occasional",
        value: "3"
      },
      {
        label: "Abundant",
        value: "3"
      },
      {
        label: "Unknown",
        value: "5"
      }
    ]
  };

const histChartConfig = {
    type: 'column2d', // The chart type
    width: '700', // Width of the chart
    height: '400', // Height of the chart
    dataFormat: 'json', // Data type
    dataSource: {
        chart: {
            caption: "Number of Historical Figures Born in Each Century",
            xAxisName: "Century",
            yAxisName: "Number of Historical Figures",
            legendposition: "bottom",
            usedataplotcolorforlabels: "1",
            theme: "fusion"

        },

        data: [
            {
                    label: "1500s",
                    value: "5"
            },
            {
                    label: "1600s",
                    value: "1"
            },
            {
                    label: "1700s",
                    value: "18"
            },
            {
                    label: "1800s",
                    value: "50"
            },
            {
                    label: "1900s",
                    value: "7"
            }
        ]
    }
};
function Visualizations () {
    const fadeIn = useSpring({
        from: {
          opacity: 0
        },
        to: {
          opacity: 1
        },
        config: { 
          duration: 2000 
        }
      });
    

        return (<>
            <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
                <animated.div style={fadeIn}>Visualizations</animated.div>
            </h1>
            <ReactFC {...chartConfigs} />
            <br></br>
            <br></br>
            <ReactFC {...histChartConfig} />
            <br></br>
            <br></br>
            <ReactFusioncharts
                type="pie2d"
                width="75%"
                height="75%"
                dataFormat="JSON"
                dataSource={dataSource} /></>
        );
}

export default Visualizations;