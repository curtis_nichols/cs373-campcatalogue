import React from "react";
import "./App.css";
import { Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, Hits, SearchBox, connectHighlight } 
from 'react-instantsearch-dom';
import * as serviceWorker from "./serviceWorker";
import {LinkContainer} from "react-router-bootstrap";
import { useSpring, animated } from "react-spring";

function Search() {

  const fadeIn = useSpring({
    from: {
      opacity: 0
    },
    to: {
      opacity: 1
    },
    config: { 
      duration: 2000 
    }
  });
  
    serviceWorker.unregister();
    
      const searchClient = 
      algoliasearch("9CK478JIC6", "36a4d2499d2516462da075b6dcd34c8c");
      
      return (
        <div>
            <h1 style={{ color: '#1B4628', backgroundColor: '#F0F0A1' }}>
              <animated.div style={fadeIn}>Search</animated.div>
            </h1>
            <InstantSearch indexName="Merged Data" searchClient={searchClient}>
            <div className="right-panel">
                <SearchBox />
                <Hits hitComponent={Hit} />
            </div>
            </InstantSearch>
        </div>
      );
    }
    
    // Highlight the words searched for using CustomHighlight
    const CustomHighlight = connectHighlight(
    ({ highlight, attribute, hit }) => {
      
    
      if(hit.scientific_name != null)
      {
        attribute="common_name";
        const parsedHit = highlight({
          highlightProperty: '_highlightResult',
          attribute,
          hit
        });
        return(
          <div>
            <img height="25%" width="25%" src={hit.image}/>
            <LinkContainer to={`/wildlife/${hit.scientific_name}`}>
              <Nav.Link>
                {parsedHit.map(part => part.isHighlighted ? 
                <mark>{part.value}</mark> : part.value)}
              </Nav.Link>
            </LinkContainer>
          </div>
        );
      }

      // Return link to historical person requested
      else if(hit.full_name != null)
      {
        attribute="full_name";
        const parsedHit = highlight({
          highlightProperty: '_highlightResult',
          attribute,
          hit
        });
        return(
          <div>
            <img height="25%" width="25%" src={hit.image}/>
            <LinkContainer to={`/history/${hit.id}`}>
              <Nav.Link>
                {parsedHit.map(part => part.isHighlighted ? 
                <mark>{part.value}</mark> : part.value)}
              </Nav.Link>
            </LinkContainer>
          </div>
        );
      }

      // Return link to park requested
      else {
        attribute="name";
        const parsedHit = highlight({
          highlightProperty: '_highlightResult',
          attribute,
          hit
        });
        return (
        <div>
          <img height="25%" width="25%" src={hit.image}/>
          <LinkContainer to={`/parks/${hit.code}`}>
            <Nav.Link>
              {parsedHit.map(part => part.isHighlighted ? 
              <mark>{part.value}</mark> : part.value)}
            </Nav.Link>
          </LinkContainer>
        </div>
        );
      }
    });
    
    const Hit = (({ hit } : {hit:any}) => {
      return(
      <div>
        <CustomHighlight attribute="name" hit={hit} />
      </div>
      );
    }
    );
    
    export default Search;