import React from "react";
import ReactDOM from "react-dom";
import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import FusionMaps from "fusioncharts/maps/fusioncharts.usa";
import World from "fusioncharts/fusioncharts.maps";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import ReactFusioncharts from "react-fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import { StayPrimaryLandscape } from "@material-ui/icons";
import { useSpring, animated } from "react-spring";

ReactFC.fcRoot(FusionCharts, FusionMaps, World, FusionTheme);
charts(FusionCharts);
function ProviderVisualizations () {

    const fadeIn = useSpring({
        from: {
          opacity: 0
        },
        to: {
          opacity: 1
        },
        config: { 
          duration: 2000 
        }
      });

    const bar = {
        type: 'column2d', // The chart type
        width: '700', // Width of the chart
        height: '400', // Height of the chart
        dataFormat: 'json', // Data type
        dataSource: {
            chart: {
                caption: "Number of Recipes by Diet",
                xAxisName: "Diets",
                yAxisName: "Number of Recieps",
                legendposition: "bottom",
                usedataplotcolorforlabels: "1",
                theme: "fusion"
    
            },
        data: [{
            label: "Paleo",
            value: 63
        },
        {
            label: "Primal",
            value: 96
        },
        {
            label: "Gluten Free",
            value: 422
        },
        {
            label: "Pescatarian",
            value: 81
        },
        {
            label: "Vegan",
            value: 161
        },
    
    ]
        }
    }
    

    const pie = {
        chart: {
            caption: "Workouts by Category",
            plottooltext: 
            "<b>$percentValue</b> of workouts in the database are $label",
            showlegend: "1",
            showpercentvalues: "1",
            legendposition: "bottom",
            usedataplotcolorforlabels: "1",
            theme: "fusion"
          },
        data: [{
                label: "Abs",
                value: 51
            },
            {
                label: "Arms",
                value: 73
            },
            {
                label: "Back",
                value: 70
            },
            {
                label: "Calves",
                value: 12
            },
            {
                label: "Chest",
                value: 55
            },
            {
                label: "Legs",
                value: 72
            },
            {
                label: "Shoulders",
                value: 54
            }]
            
    }
    
    const line =  {
        type: 'line',
        width: '700',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            chart: {
                caption: "News Articles Published by Week",
                xAxisName: "Week",
                yAxisName: "Number of Articles",
                theme: "fusion",
                lineThickness: "2"
            },
        data: [{
                label: "09/21 - 09/27",
                value: 45
            },
            {
                label: "09/28 - 10/04",
                value: 24
            },
            {
                label: "10/05 - 10/11",
                value: 15
            },
            {
                label: "10/12 - 10/18",
                value: 51
            },
            {
                label: "10/19 - 10/25",
                value: 25
            } 
        ],
    
        trendlines: [{
            line: [{
                    startvalue: "32",
                    color: "#29C3BE",
                    displayvalue: "Average{br}weekly{br}articles",
                    valueOnRight: "1",
                    thickness: "2"
                }]
            }]
        }
    }
    
    
    
    return (
        <><h1 style={{ color: '#1B4628', backgroundColor: '#F0F0A1' }}>
            <animated.span style={fadeIn}>Provider Visualizations</animated.span>
        </h1>
            <ReactFusioncharts
                type="pie2d"
                width="100%"
                heigth="200%"
                dataFormat="JSON"
                dataSource={pie} />
            <br></br>
            <br></br>
            <ReactFC {...bar} />                 
            <br></br>
            <br></br>
            <ReactFC {...line} />  
        </>
    );
}

export default ProviderVisualizations;