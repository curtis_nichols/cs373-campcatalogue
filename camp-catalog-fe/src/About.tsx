import React, { Component } from "react";
import { Card, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import curtisImg from "./img/curtis-img.jpg";
import nikhilaImg from "./img/nikhila-img.jpg";
import benImg from "./img/ben-img.jpg";
import grantImg from "./img/grant-img.png";
import connerImg from "./img/conner-img.jpg";
import reactImg from "./img/react-img.png";
import flaskImg from "./img/flask-img.png";
import sqlalchemyImg from "./img/sqlalchemy.jpg";
import mochaImg from "./img/mocha-img.png";
import pytestImg from "./img/pytest-img.png";
import seleniumImg from "./img/selenium-img.png";
import postmanImg from "./img/postman-img.jpg";
import dbeaverImg from "./img/dbeaver-img.png";
import gitlabImg from "./img/gitlab-img.jpeg";
import npsImg from "./img/nps-img.jpg";
import irmaImg from "./img/irma-img.jpg";
import phylopicImg from "./img/phylopic-img.png";
import { LinearProgress } from "@material-ui/core";
import { useSpring, animated } from 'react-spring';
import { useQuery} from 'react-query'


    const QueryGitlab = () => {
      const commit = useQuery('commits', () =>
      fetch(
        'https://gitlab.com/api/v4/projects/21326100/repository/commits?per_page=100'
        ).then(res =>
        res.json()
      ))
      const issue = useQuery('issues', () =>
      fetch(
        'https://gitlab.com/api/v4/projects/21326100/issues?per_page=100'
        ).then(res =>
        res.json()
      ))
      return [commit,issue];
    }


    function About () {

      const fadeIn = useSpring({
        from: {
          opacity: 0
        },
        to: {
          opacity: 1
        },
        config: { 
          duration: 2000 
        }
      });

      const [
        { isLoading: commit_loading, error: commit_error, data: commitData }, 
        { isLoading: issue_loading, error: issue_error, data: issueData } 
      ] = QueryGitlab()
  
      /**
     * Check for error reading from API
     */
    if(commit_error || issue_error){
      return (
        <div>
          <h1>Error reading API</h1>
        </div>
      );
    }
        /**
      * While the data is not loaded, a spinner
      * will run to indicate it is being loaded
      */
    else if(commit_loading || issue_loading){
      return (
        <div>
          <LinearProgress color="primary"/>
        </div>
      );
    }
    else{
    var team_members = 5;
    var commitLog = commitData.map(
      (item: any) => item.committer_name
    );
    
    var issueLog = issueData.map((item: any) =>
      item.closed_by != null ? item.closed_by.name : null
    );
    
    
    // Sets up the dynamic data counts
    var commit_counts = [0, 0, 0, 0, 0];
    var issue_counts = [0, 0, 0, 0, 0];

    // Sets up the static data for about statistics
    var usernames = [
      "Grant Larson",
      "benmmiller2018",
      "Curtis J. Nichols",
      "connerpope",
      "nikhilatexas",
    ];
    var names = [
      "Grant Larson",
      "Benjamin Miller",
      "Curtis Nichols",
      "Conner Pope",
      "Nikhila Ravikumar",
    ];
    var pics = [grantImg, benImg, curtisImg, connerImg, nikhilaImg];
    var fav_colors = ["Blue", "Red", "Red", "Blue", "Lavender"];
    var home_towns = [
      "Austin, TX",
      "Sugar Land, TX",
      "Washington, DC",
      "Lubbock, TX",
      "Austin, TX",
    ];
    var linkedInLinks = [
      "https://www.campcatalog.me/#/about",
      "https://www.campcatalog.me/#/about",
      "https://www.linkedin.com/in/curtis-j-nichols/",
      "https://www.campcatalog.me/#/about",
      "https://www.linkedin.com/in/nikhila-ravikumar/",
    ];
    var responsibilities = [
      "API, Testing, Docker, general backend development",
      "Provider User Stories, Testing, AWS/hosting, general frondend/backend",
      "About Page, Testing, Search functionality, general frontend development",
      "Frontend(instance pages, pagination, filtering, sorting), finding"
        + " APIs and filling database, managing AWS (setting up RDS, Route 53,"
        + " and deployment on EC2)",
      "Frontend development (model pages, instance pages, navbar);"
        + " backend development (DBeaver and Postman to fill up database);"
        + " Technical Report and assigning issues",
    ];

    /**
     * Counts the number of issues each team member closed
     * and updates the issue counts dynamically
     */
    var matched = false;
    for (var i = 0; i < issueLog.length; ++i) {
      matched = false;
      for (var j = 0; j < team_members && !matched; ++j) {
        if (
          issueLog[i] + " " === usernames[j] + " " ||
          issueLog[i] + " " === names[j] + " "
        ) {
          matched = true;
          ++issue_counts[j];
        }
      }
    }

    /**
     * Counts the number of commits each team member
     * committed and updates the commit counts dynamically
     */
    matched = false;
    for (i = 0; i < commitLog.length; ++i) {
      matched = false;
      for (j = 0; j < team_members && !matched; ++j) {
        if (
          commitLog[i] + " " === usernames[j] + " " ||
          commitLog[i] + " " === names[j] + " "
        ) {
          matched = true;
          ++commit_counts[j];
        }
      }
    }


    const plainText = {
      textDecoration: 'none',
      color: 'black'
    }

    return (
      <div className="App">
        <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
          <animated.span style={fadeIn}>About</animated.span>
        </h1>
        {/* THE BELOW SHOULD BE A LOOP;
         Adds react cards for each team member with stats */}
        <Container fluid>
          <Row className="card-deck">
            <Card border="light">
              <Card.Body>
                <Card.Img src={pics[0]} style={{ width: "80%", borderRadius: '25px'}}>
                </Card.Img>
                <Card.Title>{names[0]}</Card.Title>
                <Card.Text>
                  Favorite Color: {fav_colors[0]} 
                  // Hometown: {home_towns[0]}{" "}
                </Card.Text>
                <Card.Text>
                  Responsibilities: {responsibilities[0]}
                </Card.Text>
                <Card.Text>
                  Commits: {commit_counts[0]} | Issues: {issue_counts[0]} | Unit
                  Tests: 0{" "}
                </Card.Text>
                <Card.Text>
                  Team Lead: Phase 4
                </Card.Text>
              </Card.Body>
            </Card>

            <Card border="light">
              <Card.Body>
                <Card.Img src={pics[1]} style={{ width: "80%", borderRadius: '25px' }}>
                </Card.Img>
                <Card.Title>{names[1]}</Card.Title>
                <Card.Text>
                  Favorite Color: {fav_colors[1]} 
                  // Hometown: {home_towns[1]}{" "}
                </Card.Text>
                <Card.Text>
                  Responsibilities: {responsibilities[1]}
                </Card.Text>
                <Card.Text>
                  Commits: {commit_counts[1]} | Issues: {issue_counts[1]} | Unit
                  Tests: 0{" "}
                </Card.Text>
                <Card.Text>
                  Team Lead: Phase 4
                </Card.Text>
              </Card.Body>
            </Card>

            <Card border="light">
              <Card.Body>
                <Card.Img src={pics[2]} style={{ width: "80%", borderRadius: '25px' }}>
                </Card.Img>
                <a href={linkedInLinks[2]}>
                  <Card.Title>{names[2]}</Card.Title>
                </a>
                <Card.Text>
                  Favorite Color: {fav_colors[2]} 
                  // Hometown: {home_towns[2]}{" "}
                </Card.Text>
                <Card.Text>
                  Responsibilities: {responsibilities[2]}
                </Card.Text>
                <Card.Text>
                  Commits: {commit_counts[2]} | Issues: {issue_counts[2]} | Unit
                  Tests: 0{" "}
                </Card.Text>
                <Card.Text>
                  Team Lead: Phase 1
                </Card.Text>
              </Card.Body>
            </Card>
          </Row>

          <Row className="card-deck">
            <Card border="light">
              <Card.Body>
                <Card.Img src={pics[3]} style={{ width: "50%", borderRadius: '25px' }}>
                </Card.Img>
                <Card.Title>{names[3]}</Card.Title>
                <Card.Text>
                  Favorite Color: {fav_colors[3]} 
                  // Hometown: {home_towns[3]}{" "}
                </Card.Text>
                <Card.Text>
                  Responsibilities: {responsibilities[3]}
                </Card.Text>
                <Card.Text>
                  Commits: {commit_counts[3]} | Issues: {issue_counts[3]} | Unit
                  Tests: 0{" "}
                </Card.Text>
                <Card.Text>
                  Team Lead: Phase 2
                </Card.Text>
              </Card.Body>
            </Card>

            <Card border="light">
              <Card.Body>
                <Card.Img src={pics[4]} style={{ width: "50%", borderRadius: '25px'}}>
                </Card.Img>
                <a href={linkedInLinks[4]}>
                  <Card.Title>{names[4]}</Card.Title>
                </a>
                <Card.Text>
                  Favorite Color: {fav_colors[4]} 
                  // Hometown: {home_towns[4]}{" "}
                </Card.Text>
                <Card.Text>
                  Responsibilities: {responsibilities[4]}
                </Card.Text>
                <Card.Text>
                  Commits: {commit_counts[4]} | Issues: {issue_counts[4]} | Unit
                  Tests: 0{" "}
                </Card.Text>
                <Card.Text>
                  Team Lead: Phase 3
                </Card.Text>
              </Card.Body>
            </Card>
          </Row>
        </Container>

        <br></br>

        {/* Lists tools used and provides description of the tool */}
        <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
          <animated.span style={fadeIn}>Tools</animated.span>
        </h1>
        <Container fluid>
          <Row className="card-deck">

            <Card border="light">
              <a style={plainText} href="https://reactjs.org/">
                <Card.Body>
                  <Card.Img src={reactImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>React</Card.Title>
                  <Card.Text>React is a front end JavaScript library 
                    used to build UI components</Card.Text>
                </Card.Body>
              </a>
            </Card>


            <Card border="light">
              <a style={plainText} href=
              "https://flask.palletsprojects.com/en/1.1.x/">
                <Card.Body>
                  <Card.Img src={flaskImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Flask</Card.Title>
                  <Card.Text>Flask is a webframework written in 
                    Python</Card.Text>
                </Card.Body>
              </a>
            </Card>

            <Card border="light">
              <a style={plainText} href="https://www.sqlalchemy.org/">
                <Card.Body>
                  <Card.Img src={sqlalchemyImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Flask-SQLAlchemy</Card.Title>
                  <Card.Text>Flask-SQLAlchemy adds support for 
                    SQLAlchemy to the web application</Card.Text>
                </Card.Body>
              </a>
            </Card>
          </Row>

          <Row className="card-deck">
            <Card border="light">
              <a style={plainText} href="https://mochajs.org/">
                <Card.Body>
                  <Card.Img src={mochaImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Mocha</Card.Title>
                  <Card.Text>Mocha is a JavaScript test framework 
                    that runs on Node.js</Card.Text>
                </Card.Body>
              </a>
            </Card>

            <Card border="light">
              <a  style={plainText} href="https://docs.pytest.org/en/stable/">
                <Card.Body>
                  <Card.Img src={pytestImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Pytest</Card.Title>
                  <Card.Text>Pytest is a testing framework that 
                    uses Python</Card.Text>
                </Card.Body>
              </a>
            </Card>

            <Card border="light">
              <a style={plainText} href="https://www.selenium.dev/">
                <Card.Body>
                  <Card.Img src={seleniumImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Selenium</Card.Title>
                  <Card.Text>Selenium helps automate web applications 
                    with regards to testing</Card.Text>
                </Card.Body>
              </a>
            </Card>
          </Row>

          <Row className="card-deck">
            <Card border="light">
                <Card.Body>
                  <Card.Img src={postmanImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>Postman</Card.Title>
                  <Card.Text>
                    Postman is an API client that allows developers 
                    to design, share, and test APIs
                  </Card.Text>
                  <Card.Text>See our Postman here:</Card.Text>
                  <Card.Link href=
                  "https://documenter.getpostman.com/view/12862819/TVYC8yrL">
                    <p>RESTful API Documentation</p>
                  </Card.Link>
                </Card.Body>
            </Card>

            <Card border="light">
              <a style={plainText} href="https://dbeaver.io/">
                <Card.Body>
                  <Card.Img src={dbeaverImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>DBeaver</Card.Title>
                  <Card.Text>DBeaver is a database tool for developers 
                    that supports multiple popular relational 
                    databases</Card.Text>
                </Card.Body>
              </a>
            </Card>

            <Card border="light">
                <Card.Body>
                  <Card.Img src={gitlabImg} style={{ width: "80%" }}>
                  </Card.Img>
                  <Card.Title>GitLab</Card.Title>
                  <Card.Text>
                    GitLab is a web-based lifecycle tool that provides 
                    features like issue tracking and continuous integration
                  </Card.Text>
				          <Card.Text>Check out our project code:</Card.Text>
                  <Card.Link href=
                  "https://gitlab.com/curtis_nichols/cs373-campcatalogue">
                    <p>GitLab</p>
                  </Card.Link>
                </Card.Body>
            </Card>
          </Row>
        </Container>

        <br></br>

        <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
          <animated.span style={fadeIn}>Data Sources</animated.span>
        </h1>
        <Container fluid>
          <Row className="card-deck">
            <Card border="light">
              <Card.Body>
                <Card.Img src={npsImg} style={{ width: "80%" }}>
                </Card.Img>
                <Card.Title>NPS Database</Card.Title>
                <Card.Text>
                  The NPS Data API contains detailed information about US 
                  national parks. The data for the parks and history models 
                  were collected from this database.
                </Card.Text>
                <Card.Link href=
                "https://www.nps.gov/subjects/developer/api-documentation.htm">
                  <p>NPS Database</p>
                </Card.Link>
              </Card.Body>
            </Card>

            <Card border="light">
              <Card.Body>
                <Card.Img src={irmaImg} style={{ width: "80%" }}>
                </Card.Img>
                <Card.Title>Irma Services Database</Card.Title>
                <Card.Text>
                  The Irma Services API contains information about the species 
                  found in US national parks. The data for the animals model 
                  was collected from this database.
                </Card.Text>
                <Card.Link href=
                "https://irmaservices.nps.gov/v3/rest/NPSpecies/help">
                  <p>Irma Services Database</p>
                </Card.Link>
              </Card.Body>
            </Card>

            <Card border="light">
              <Card.Body>
                <Card.Img src={phylopicImg} style={{ width: "80%" }}>
                </Card.Img>
                <Card.Title>Phylopic Database</Card.Title>
                <Card.Text>
                  The Phylopic API contains images of the species found in 
                  US national parks. The images for the animals model was 
                  collected from this database.
                </Card.Text>
                <Card.Link href=
                "http://phylopic.org/api/">
                  <p>Phylopic Database</p>
                </Card.Link>
              </Card.Body>
            </Card>
          </Row>
        </Container>
      </div>
    );
  }
}

export default About;
