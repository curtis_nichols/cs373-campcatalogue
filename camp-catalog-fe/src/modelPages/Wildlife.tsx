import React,{ Component } from "react";
import { Button, Card } from "react-bootstrap";
import MaterialTable from "material-table";
import { Link, Switch, Route } from "react-router-dom";
import { LinearProgress } from "@material-ui/core";
import { useSpring, animated } from 'react-spring';
import { useQuery } from 'react-query';

/**
 * The Wildlife function is a component that handles
 * loading and displaying the data for the wildlife
 * model page.
 */
function Wildlife(){

  const fadeIn = useSpring({
    from: {
      opacity: 0
    },
    to: {
      opacity: 1
    },
    config: { 
      duration: 2000 
    }
  });

  /**
   * fetches API data and load data and status into variables.
   * caching is handled by query with key word access
   */
  
  const { isLoading, error, data } = useQuery('wildlife', () =>
    fetch('/api/wildlife').then(res =>
      res.json()
    )
  )


  /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }
  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }
    else{

      return(
        <div className="App">
          <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
            <animated.span style={fadeIn}>Wildlife</animated.span>
          </h1>
        {/**
           * Place the data in a Material Table.
           * Some columns are hidden to allow them
           * to be displayed in the detail panel.
           */}
          <MaterialTable
          options={{
            filtering: true
          }}
        columns={[
          {title: "Scientific Name", field: "scientific_name"},
          {title: "Common Names", field: "common_names"},
          {title: "Category", field: "category"},
          {title: "Order", field: "order"},
          {title: "Family", field: "family"},
          {field: "image", hidden: true},
          {title: "Nativeness", field: "nativeness"}
        ]}
        data={data.objects}
        title="Wildlife"
        detailPanel={rowData => {
         /**
           * The detail panel contains a card containing
           * an image, more details, and a button linking
           * to the instance page for piece of history data.
           */
          return (
            <Card>
                <Card.Body>
                    <img style={{width: "35%"}} src={rowData.image} />
                    <Card.Title>{rowData.common_names}</Card.Title>
                    <Card.Text>{rowData.category}</Card.Text>
                    <Card.Text>Order: {rowData.order}</Card.Text>
                    <Card.Text>Family: {rowData.family}</Card.Text>
                    <Card.Text>
                      Scientific Name: {rowData.scientific_name}
                    </Card.Text>
                    <Card.Text>Nativeness: {rowData.nativeness}</Card.Text>
                    <Link to={"/wildlife/" + rowData.scientific_name}>
                      <Button variant="primary">Learn More</Button>
                    </Link>
                </Card.Body>
            </Card>
          )
        }}
        />
        </div>
      );
    }
  }


export default Wildlife;

