import React,{ Component, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { Link, Switch, Route } from "react-router-dom";
import MaterialTable from "material-table";
import { LinearProgress } from "@material-ui/core";
import { useSpring, animated } from 'react-spring';
import { useQuery } from 'react-query';



/**
 * The History function is a component that handles
 * loading and displaying the data for the history
 * model page.
 */
function History() {

  const fadeIn = useSpring({
    from: {
      opacity: 0
    },
    to: {
      opacity: 1
    },
    config: { 
      duration: 2000 
    }
  });

  /**
   * fetches API data and load data and status into variables.
   * caching is handled by query with key word access
   */
  
  const { isLoading, error, data } = useQuery('history', () =>
    fetch('/api/historical_people').then(res =>
      res.json()
    )
  )


  /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }
  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }
  else{
    return(
      <div className="App">
        <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
          <animated.span style={fadeIn}>Historical Figures</animated.span>
        </h1>
      {/**
         * Place the data in a Material Table.
         * Some columns are hidden to allow them
         * to be displayed in the detail panel.
         */}
        <MaterialTable
        options={{
          filtering: true
        }}
      columns={[
        {title: "Name", field: "full_name"},
        {title: "Date of Birth", field: "date_of_birth"},
        {title: "Date of Death", field: "date_of_death"},
        {title: "Place of Birth", field: "place_of_birth"},
        {title: "Significance", field: "significance"},
        {field: "id", hidden: true},
        {field: "image", hidden: true},
      ]}
      data={data.objects}
      title="Historical Figures"
      detailPanel={rowData => {
       /**
         * The detail panel contains a card containing
         * an image, more details, and a button linking
         * to the instance page for piece of history data.
         */
        return (
          <Card>
              <Card.Body>
                  <img style={{width: "35%"}} src={rowData.image} />
                  <Card.Title>{rowData.full_name}</Card.Title>
                  <Card.Text>
                    {rowData.date_of_birth} - {rowData.date_of_death}
                  </Card.Text>
                  <Card.Text>From: {rowData.place_of_birth}</Card.Text>
                  <Card.Text>
                    Significance: {rowData.significance}
                  </Card.Text>
                  <Link to={"/history/" + rowData.id}>
                    <Button variant="primary">Learn More</Button>
                  </Link>
              </Card.Body>
          </Card>
        )
      }}
      />
      </div>
    );
  }

}



export default History;
