import { Button } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { Link, Route, Switch } from "react-router-dom";
import React, { Component } from "react";
import MaterialTable from "material-table";
import { LinearProgress } from "@material-ui/core";
import { useSpring, animated } from 'react-spring';
import { useQuery } from 'react-query'

/**
 * The Parks function is a component that handles
 * loading and displaying the data for the park
 * model page.
 */
function Parks() {

  const fadeIn = useSpring({
    from: {
      opacity: 0
    },
    to: {
      opacity: 1
    },
    config: { 
      duration: 2000 
    }
  });

  /**
   * fetches API data and load data and status into variables.
   * caching is handled by query with key word access
   */
  
  const { isLoading, error, data } = useQuery('parks', () =>
    fetch('/api/parks').then(res =>
      res.json()
    )
  )


  /**
   * Check for error reading from API
   */
  if(error){
    return (
      <div>
        <h1>Error reading API</h1>
      </div>
    );
  }
  /**
   * Loading bar while data is loaded
   */
  if(isLoading){
    return (
      <div>
        <LinearProgress color="primary"/>
      </div>
    );
  }
    else {
      return(
        <div className="App">
          <h1 style={{color: '#1B4628', backgroundColor: '#F0F0A1'}}>
            <animated.span style={fadeIn}>Parks</animated.span>
          </h1>
          {/**
           * Place the data in a Material Table.
           * Some columns are hidden to allow them
           * to be displayed in the detail panel.
           */}
          <MaterialTable
          options={{
            filtering: true
          }}
        columns={[
          {title: "Name", field: "name"},
          {title: "City", field: "city"},
          {title: "State", field: "state"},
          {title: "Zip Code", field: "zip"},
          {title: "Fee", field: "fee"},
          {field: "code", hidden: true},
          {field: "image", hidden: true},
          {field: "saturday_hours", hidden: true},
          {
            title: "Camping Allowed", 
            field: "camping",
            searchable: false,
            render: rowData => <p>{rowData.camping? "Yes" : "No"}</p>,
            customFilterAndSearch: (term, rowData) =>
                term === (rowData.camping ? "Yes" : "No")
          },
          {
            title: "Hiking Offered", 
            field: "hiking",
            searchable: false,
            render: rowData => <p>{rowData.hiking? "Yes" : "No"}</p>,
            customFilterAndSearch: (term, rowData) =>
                term === (rowData.hiking ? "Yes" : "No")
          }
        ]}
        data={data.objects}
        title="National Parks"
        detailPanel={rowData => {
          /**
           * The detail panel contains a card containing
           * an image, more details, and a button linking
           * to the instance page for each park.
           */
          return (
            <Card>
                <Card.Body>
                    <img style={{width: "35%"}} src={rowData.image} />
                    <Card.Title>{rowData.name}</Card.Title>
                    <Card.Text>{rowData.city}, {rowData.state}</Card.Text>
                    <Card.Text>ZIP: {rowData.zip}</Card.Text>
                    <Card.Text>
                      Saturday Hours: {rowData.saturday_hours}
                    </Card.Text>
                    <Card.Text>Entrance Fee: {rowData.fee}</Card.Text>
                    <Link to={"/parks/" + rowData.code}>
                      <Button variant="primary">Learn More</Button>
                    </Link>
                </Card.Body>
            </Card>
          )
        }}
        />
        </div>
      );
    }
  }



export default Parks;
