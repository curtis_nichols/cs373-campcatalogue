# test_gui.py: use Selenium and Pytest to ensure our GUI works as intended

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pyvirtualdisplay import Display
import time

display = Display(visible=0, size=(800, 800))
display.start()
driver = webdriver.Chrome()


def test_example():
    driver.get("https://www.python.org")
    print(driver.title)
    search_bar = driver.find_element_by_name("q")
    search_bar.clear()
    search_bar.send_keys("getting started with python")
    search_bar.send_keys(Keys.RETURN)
    print(driver.current_url)
    driver.close()


def test_landing():
    driver.get("http://campcatalog.me/#/")
    button = driver.find_element_by_name("See the Parks")
    button.click()
    time.sleep(3)
    expected_result = "Parks"
    actual_result = driver.find_element_by_class_name("PageHeader").text
    assert expected_result == actual_result
