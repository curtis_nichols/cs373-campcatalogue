// //tests.js: Unit Tests for our frontend code with Mocha; verify everything is loading error-free
var assert = require('assert');
const request = require('request');
const chai = require('chai');
var expect = chai.expect;


// // ------------
// // Define Tests
// // ------------

//Use this format for future tests
//NOTE: EXAMPLE is a testing suite, while EXAMPLE-A is one of the tests it contains

describe('Website', function () {

    //test the home page of the website
    describe('Landing Page', function () {

      var url = "http://campcatalog.me/#";
    
        it('no error should be returned', function (done) {
          request(url, function(error, response, body) {
          expect(error).to.equal(null);
          done();
          });
        });
      });

    //test the about page of the website
    describe('About Page', function () {

      var url = "http://campcatalog.me/#/about";
    
        it('no error should be returned', function (done) {
          request(url, function(error, response, body) {
          expect(error).to.equal(null);
          done();
          });
        });
      });
    
      //---------------------------------
      //test the history page of the website
      //---------------------------------
      describe('History Page', function () {
    
      var url = "http://campcatalog.me/#/history";
    
        it('no error should be returned', function (done) {
          request(url, function(error, response, body) {
          expect(error).to.equal(null);
          done();
          });
        });
      });
    
      //---------------------------------
      //test the parks page of the website
      //---------------------------------
      describe('Parks Page', function () {
    
      var url = "http://campcatalog.me/#/parks";
    
        it('no error should be returned', function (done) {
          request(url, function(error, response, body) {
          expect(error).to.equal(null);
          done();
          });
        });
      });
    
      //---------------------------------
      //test the wildlife page of the website
      //---------------------------------
      describe('Wildlife Page', function () {
    
      var url = "http://campcatalog.me/#/wildlife";
    
        it('no error should be returned', function (done) {
          request(url, function(error, response, body) {
          expect(error).to.equal(null);
          done();
          });
        });
      });

      //---------------------------------
      //test the search page of the website
      //---------------------------------
      describe('Search Page', function () {
    
        var url = "http://campcatalog.me/#/search";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the first wildlife instance page of the website
      //---------------------------------
      describe('Wildlife Instance Page', function () {
    
        var url = "http://campcatalog.me/#/wildlife/Accipiter%20cooperii";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last wildlife instance page from the model page of the website
      //---------------------------------
      describe('Wildlife Instance Page', function () {
    
        var url = "http://campcatalog.me/#/wildlife/Ursus%20americanus";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last wildlife instance page of the website
      //---------------------------------
      describe('Wildlife Instance Page', function () {
    
        var url = "http://campcatalog.me/#/wildlife/Didelphis%20virginiana";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the first park instance page of the website
      //---------------------------------
      describe('Park Instance Page', function () {
    
        var url = "http://campcatalog.me/#/parks/abli";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last park instance page from the model page of the website
      //---------------------------------
      describe('Parks Instance Page', function () {
    
        var url = "http://campcatalog.me/#/parks/choh";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last park instance page from the database of the website
      //---------------------------------
      describe('Parks Instance Page', function () {
    
        var url = "http://campcatalog.me/#/parks/zion";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the first history instance page from the model page of the website
      //---------------------------------
      describe('History Instance Page', function () {
    
        var url = "http://campcatalog.me/#/history/2";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last history instance page from the model page of the website
      //---------------------------------
      describe('History Instance Page', function () {
    
        var url = "http://campcatalog.me/#/history/101";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });

      //---------------------------------
      //test the last history instance page of the website
      //---------------------------------
      describe('History Instance Page', function () {
    
        var url = "http://campcatalog.me/#/history/201";
      
          it('no error should be returned', function (done) {
            request(url, function(error, response, body) {
            expect(error).to.equal(null);
            done();
            });
          });
        });
});