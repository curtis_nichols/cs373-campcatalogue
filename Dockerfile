FROM node:12

RUN apt-get update

RUN apt-get -y install libboost-all-dev
RUN apt-get -y install libgmp-dev
RUN apt-get -y install vim
RUN apt-get -y install python3
RUN apt-get -y install curl

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

RUN python get-pip.py
RUN pip install --upgrade pip
RUN pip --version

RUN npm i react
RUN npm i react-router-bootstrap
RUN npm i react-favicon
RUN npm i --global mocha
RUN npm i react-spring
RUN npm i react-fusioncharts
RUN npm i @types/fusioncharts

RUN pip install Flask-Restless
RUN pip install Flask-SQLAlchemy
RUN pip install flask-cors
RUN pip install psycopg2
RUN pip install pytest

RUN git clone https://gitlab.com/curtis_nichols/cs373-campcatalogue.git

WORKDIR /cs373-campcatalogue

RUN git pull --force && cd camp-catalog-fe && npm install && npm run build
RUN cd ..

EXPOSE 80

CMD git pull --force && python camp-catalog-be/app.py
