# cs373-campcatalogue

-------------------------------------------------Phase 1-------------------------------------------------
Our website for CS373: Camp Catalogue.

**Members:**

**Nikhila Ravikumar**

EID: nr23542

GitLab ID: nikhilatexas

Estimated Completion Time: 12 hours

Actual Completion Time: 14 hours

**Conner Pope**

EID: ccp999

GitLab ID: connerpope

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Grant Larson**

EID: gnl258

GitLab ID: GNLarson

Estimated Completion Time: 8 hours

Actual Completion Time: 10 hours

**Curtis Nichols**

EID: cjn857

GitLab ID: curtis_nichols

Estimated Completion Time: 10 hours

Actual Completion Time: 7 hours

**Ben Miller**

EID: bmm3746

GitLab ID: benmmiller2018

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Git SHA:** d21fbedcceb9469d5da600e7a0875dc498739d3a

**Project Leader:** Curtis Nichols

**GitLab Pipelines:** https://gitlab.com/curtis_nichols/cs373-campcatalogue/-/pipelines

**Website Link:** https://master.d23a9gchkb83zj.amplifyapp.com/home

**Comments:** Adam and Alvin attempted to help us deploy our website, but we were not successful before the deadline.

-------------------------------------------------Phase 2-------------------------------------------------
Our website for CS373: Camp Catalogue.

**Members:**

**Nikhila Ravikumar**

EID: nr23542

GitLab ID: nikhilatexas

Estimated Completion Time: 14 hours

Actual Completion Time: 10 hours

**Conner Pope**

EID: ccp999

GitLab ID: connerpope

Estimated Completion Time: 15 hours

Actual Completion Time: 15 hours

**Grant Larson**

EID: gnl258

GitLab ID: GNLarson

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Curtis Nichols**

EID: cjn857

GitLab ID: curtis_nichols

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Ben Miller**

EID: bmm3746

GitLab ID: benmmiller2018

Estimated Completion Time: 14 hours

Actual Completion Time: 12 hours

**Git SHA:** dde5a983cff3a717ac99ac090e56e19d49e71af0

**Project Leader:** Conner Pope

**GitLab Pipelines:** https://gitlab.com/curtis_nichols/cs373-campcatalogue/-/pipelines

**Website Link:** https://campcatalog.me

**Comments:** We attempted to get pagination working for the model pages, but we were unable to before the deadline. We do have 100+ instances per model page.

-------------------------------------------------Phase 3-------------------------------------------------
Our website for CS373: Camp Catalogue.

**Members:**

**Nikhila Ravikumar**

EID: nr23542

GitLab ID: nikhilatexas

Estimated Completion Time: 10 hours

Actual Completion Time: 8 hours

**Conner Pope**

EID: ccp999

GitLab ID: connerpope

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Grant Larson**

EID: gnl258

GitLab ID: GNLarson

Estimated Completion Time: 7 hours

Actual Completion Time: 5 hours

**Curtis Nichols**

EID: cjn857

GitLab ID: curtis_nichols

Estimated Completion Time: 8 hours

Actual Completion Time: 12 hours

**Ben Miller**

EID: bmm3746

GitLab ID: benmmiller2018

Estimated Completion Time: 8 hours

Actual Completion Time: 6 hours

**Git SHA:** f7d1a37815d79f3698a7d67f3eae7e289e66d45b

**Project Leader:** Nikhila Ravikumar

**GitLab Pipelines:** https://gitlab.com/curtis_nichols/cs373-campcatalogue/-/pipelines

**Website Link:** https://campcatalog.me

**Comments:** Concerning the favicon, our group wasn't able to get the new image working.
Our group discussed it with Larry and he said we would not
lose points for it.

    	We have a few lines of code over 80 characters,
    	which are URLS we used in the frontend.
    	We weren't able to break up the URLs without
    	breaking the images on the site.

    	Additionally, we have one console error about 'manifest.json'.
    	We haven't been able to figure out what this error means or how
    	to get rid of it.

-------------------------------------------------Phase 4-------------------------------------------------
Our website for CS373: Camp Catalogue.

**Members:**

**Nikhila Ravikumar**

EID: nr23542

GitLab ID: nikhilatexas

Estimated Completion Time: 10 hours

Actual Completion Time: 12 hours

**Conner Pope**

EID: ccp999

GitLab ID: connerpope

Estimated Completion Time: 8 hours

Actual Completion Time: 8 hours

**Grant Larson**

EID: gnl258

GitLab ID: GNLarson

Estimated Completion Time: 12 hours

Actual Completion Time: 12 hours

**Curtis Nichols**

EID: cjn857

GitLab ID: curtis_nichols

Estimated Completion Time: 8 hours

Actual Completion Time: 6 hours

**Ben Miller**

EID: bmm3746

GitLab ID: benmmiller2018

Estimated Completion Time: 8 hours

Actual Completion Time: 8 hours

**Git SHA:** c42c388d6ac669f16b4d0a00c99b7019cf3f8385

**Project Leader:** Grant Larson & Benjamin Miller

**GitLab Pipelines:** https://gitlab.com/curtis_nichols/cs373-campcatalogue/-/pipelines

**Website Link:** https://campcatalog.me

**Comments:** Thank you for all the office hours and grading our project!
