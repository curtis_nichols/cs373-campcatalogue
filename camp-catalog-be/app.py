from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager
from flask import Flask, render_template, request, send_from_directory
from flask_cors import CORS

app = Flask(
    __name__,
    static_folder="../camp-catalog-fe/build/static",
    template_folder="../camp-catalog-fe/build",
)

app.config.from_pyfile("config.cfg")

CORS(app)

db = SQLAlchemy(app)

# Define the bridge tables between our models

parks_history = db.Table(
    "parks_history",
    db.Column("park_code", db.String(4), db.ForeignKey("parks.code")),
    db.Column("history_id", db.Integer, db.ForeignKey("historical_people.id")),
)

parks_wildlife = db.Table(
    "parks_wildlife",
    db.Column("park_code", db.String(4), db.ForeignKey("parks.code")),
    db.Column(
        "wildlife_scientific_name",
        db.String(50),
        db.ForeignKey("wildlife.scientific_name"),
    ),
)

# Define the models themselves

class historical_people(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    middle_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    date_of_birth = db.Column(db.String(50))
    date_of_death = db.Column(db.String(50))
    place_of_birth = db.Column(db.String(200))
    significance = db.Column(db.String(1000))
    description = db.Column(db.String(1000))
    image = db.Column(db.String(1000))
    full_name = db.Column(db.String(200))


class parks(db.Model):
    code = db.Column(db.String(4), primary_key=True)
    name = db.Column(db.String(200))
    city = db.Column(db.String(50))
    state = db.Column(db.String(50))
    fee = db.Column(db.String(50))
    zip = db.Column(db.String(50))
    sunday_hours = db.Column(db.String(50))
    monday_hours = db.Column(db.String(50))
    tuesday_hours = db.Column(db.String(50))
    wednesday_hours = db.Column(db.String(50))
    thursday_hours = db.Column(db.String(50))
    friday_hours = db.Column(db.String(50))
    saturday_hours = db.Column(db.String(50))
    description = db.Column(db.String(1000))
    image = db.Column(db.String(1000))
    longitude = db.Column(db.Float(10))
    latitude = db.Column(db.Float(10))
    hiking = db.Column(db.Boolean)
    camping = db.Column(db.Boolean)
    food = db.Column(db.Boolean)
    related_historical_figures = db.relationship(
        "historical_people",
        secondary=parks_history,
        backref=db.backref("related_parks"),
    )
    related_wildlife = db.relationship(
        "wildlife",
        secondary=parks_wildlife,
        backref=db.backref("related_parks"),
    )


class wildlife(db.Model):
    scientific_name = db.Column(db.String(200), primary_key=True)
    common_names = db.Column(db.String(200))
    family = db.Column(db.String(200))
    abundance = db.Column(db.String(50))
    category = db.Column(db.String(50))
    order = db.Column(db.String(200))
    nativeness = db.Column(db.String(50))
    image = db.Column(db.String(100))
    image_source = db.Column(db.String(50))
    photographer = db.Column(db.String(50))

manager = APIManager(app, flask_sqlalchemy_db=db)

manager.create_api(historical_people, results_per_page=100)
manager.create_api(parks, results_per_page=100)
manager.create_api(wildlife, results_per_page=100)
    
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=False)
