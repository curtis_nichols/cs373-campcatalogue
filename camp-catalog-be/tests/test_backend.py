# tests.py: Unit Tests for our backend code, with pytest; primarily testing API calls with request
# ------------
# Define Tests
# ------------
import requests

base_url = "http://campcatalog.me/api/"

# test_example: filler test to test system
def test_example():
    assert True


def test_animal_results():
    r = requests.get(base_url + "wildlife")
    assert r.status_code == 200


def test_animal_num():
    r = requests.get(base_url + "wildlife")
    assert r.status_code == 200
    j = r.json()
    assert len(j["objects"]) > 99
    assert j["num_results"] == 103

def test_animal_example():
    r = requests.get(base_url + "wildlife")
    assert r.status_code == 200
    j = r.json()
    assert len(j["objects"]) > 99
    assert j["objects"][0] == {
      "scientific_name": "Accipiter cooperii", 
      "common_names": "Cooper's Hawk", 
      "family": "Accipitridae", 
      "abundance": "Uncommon", 
      "category": "Bird", 
      "order": "Accipitriformes", 
      "nativeness": "Native", 
      "image": "https://upload.wikimedia.org/wikipedia/commons/f/f5/CoopersHawk.JPG", 
      "image_source": "commons.wikimedia.org", 
      "photographer": "A. Stanley", 
      "related_parks": [
        {
          "code": "yell", 
          "name": "Yellowstone National Park", 
          "city": "Yellowstone National Park", 
          "state": "WY", 
          "fee": "$35.00", 
          "zip": "82190", 
          "sunday_hours": "All Day", 
          "monday_hours": "All Day", 
          "tuesday_hours": "All Day", 
          "wednesday_hours": "All Day", 
          "thursday_hours": "All Day", 
          "friday_hours": "All Day", 
          "saturday_hours": "All Day", 
          "description": "On March 1, 1872, Yellowstone became the first national park for all to enjoy the unique hydrothermal wonders. Today, millions of people come here each year to camp, hike, and enjoy the majesty of the park.", 
          "image": "https://www.nps.gov/common/uploads/structured_data/3C7D2FBB-1DD8-B71B-0BED99731011CFCE.jpg", 
          "longitude": -110.5471695, 
          "latitude": 44.59824417, 
          "hiking": True, 
          "camping": True, 
          "food": True
        }
        ]
    }

def test_park_results():
    r = requests.get(base_url + "parks")
    assert r.status_code == 200


def test_park_num():
    r = requests.get(base_url + "parks")
    assert r.status_code == 200
    j = r.json()
    assert len(j["objects"]) > 99
    assert j["num_results"] == 497

def test_park_example():
    r = requests.get(base_url + "parks")
    assert r.status_code == 200
    j = r.json()
    assert j["objects"][0] == {
      "code": "abli", 
      "name": "Abraham Lincoln Birthplace National Historical Park", 
      "city": "Hodgenville", 
      "state": "KY", 
      "fee": "$0.00", 
      "zip": "42748", 
      "sunday_hours": "9:00AM - 5:00PM", 
      "monday_hours": "9:00AM - 5:00PM", 
      "tuesday_hours": "9:00AM - 5:00PM", 
      "wednesday_hours": "9:00AM - 5:00PM", 
      "thursday_hours": "9:00AM - 5:00PM", 
      "friday_hours": "9:00AM - 5:00PM", 
      "saturday_hours": "9:00AM - 5:00PM", 
      "description": "For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.", 
      "image": "https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg", 
      "longitude": -85.67330523, 
      "latitude": 37.5858662, 
      "hiking": False, 
      "camping": False, 
      "food": True, 
      "related_wildlife": [], 
      "related_historical_figures": [
        {
          "id": 43, 
          "first_name": "", 
          "middle_name": "", 
          "last_name": "", 
          "date_of_birth": "February 12, 1809", 
          "date_of_death": "April 15, 1865", 
          "place_of_birth": "Hodgenville, KY", 
          "significance": "President of the United States", 
          "description": "President Lincoln's leadership of the United States of America through the cataclysm of the Civil War ranks as one of the finest presidencies in American history.", 
          "image": "https://www.nps.gov/common/uploads/people/images/nri/subject/civilwar/C7D90BDB-023E-F59D-053AB5C865193C24/C7D90BDB-023E-F59D-053AB5C865193C24.jpg", 
          "full_name": "Abraham Lincoln"
        }
      ]
    }



def test_history_results():
    r = requests.get(base_url + "historical_people")
    assert r.status_code == 200


def test_history_num():
    r = requests.get(base_url + "historical_people")
    assert r.status_code == 200
    j = r.json()
    assert len(j["objects"]) > 99
    assert j["num_results"] == 200


def test_history_example():
    r = requests.get(base_url + "historical_people")
    assert r.status_code == 200
    j = r.json()
    assert j["objects"][0] == {
      "id": 2, 
      "first_name": "Pauline", 
      "middle_name": "Cushman", 
      "last_name": "Fryer", 
      "date_of_birth": "June 10, 1833", 
      "date_of_death": "December 1, 1893", 
      "place_of_birth": "New Orleans, LA", 
      "significance": "Civil War Spy", 
      "description": "Actress and Civil War spy, Pauline Cushman-Fryer narrowly escaped execution for her service to the Union cause. Undercover in Tennessee she performed an illness to escape hanging. She is buried in the Officer's section of the San Francisco National Cemetery at the Presidio.", 
      "image": "https://www.nps.gov/common/uploads/people/nri/20200814/people/34BCEDFA-D585-62B8-086046B6CEF7BACC/34BCEDFA-D585-62B8-086046B6CEF7BACC.jpg", 
      "full_name": "Pauline Cushman", 
      "related_parks": [
        {
          "code": "goga", 
          "name": "Golden Gate National Recreation Area", 
          "city": "San Francisco", 
          "state": "CA", 
          "fee": "$0.00", 
          "zip": "94123-0022", 
          "sunday_hours": "All Day", 
          "monday_hours": "All Day", 
          "tuesday_hours": "All Day", 
          "wednesday_hours": "All Day", 
          "thursday_hours": "All Day", 
          "friday_hours": "All Day", 
          "saturday_hours": "All Day", 
          "description": "Experience a park so rich it supports 19 distinct ecosystems with over 2,000 plant and animal species. Go for a hike, enjoy a vista, have a picnic or learn about the centuries of overlapping history from California\u2019s indigenous cultures, Spanish colonialism, the Mexican Republic, US military expansion and the growth of San Francisco. All of this and more awaits you, so get out and find your park.", 
          "image": "https://www.nps.gov/common/uploads/structured_data/3C7FE3B8-1DD8-B71B-0B91991C4D692710.jpg", 
          "longitude": -122.6007386, 
          "latitude": 37.85982543, 
          "hiking": True, 
          "camping": True, 
          "food": True
        }
      ]
    }
