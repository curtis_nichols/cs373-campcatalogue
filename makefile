.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3
    NODE          := node
    MOCHA         := node_modules/mocha/bin/mocha
    PYTEST        := pytest
    CHROMEDRIVER  := camp-catalog-fe/tests/chromedriver
    NPM           := npm
else ifeq ($(shell uname -p), unknown)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc
    PYLINT        := pylint
    PYTHON        := python
    NODE          := node
    MOCHA         := node_modules/mocha/bin/mocha
    PYTEST        := pytest
    CHROMEDRIVER  := camp-catalog-fe/tests/chromedriver
    NPM           := npm
else
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint3
    PYTHON        := python3
    NODE          := node
    MOCHA         := node_modules/mocha/bin/mocha
    PYTEST        := pytest
    CHROMEDRIVER  := camp-catalog-fe/tests/chromedriver
    NPM           := npm
endif

# #run test files for frontend
test-fe : 
	$(MOCHA) camp-catalog-fe/tests

#run test files for backend
test-be : 
	$(PYTHON) -m $(PYTEST) camp-catalog-be/tests

#source define_vars.sh
#to run this, first use: $export PATH=$PATH:$(pwd):camp-catalog-fe/tests
test-gui : 
	$(PYTHON) -m $(PYTEST) camp-catalog-fe/tests

test-all : 
	make test-fe
	make test-be

start :
	#$(NPM) run build ./camp-catalog-fe
	cd camp-catalog-fe/ && $(NPM) run build
	$(PYTHON) ./camp-catalog-be/app.py